const startUpdebug = require("debug")("app:startup");
var bodyParser = require("body-parser");
const config = require("config");
var express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

//Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "MCC & MNC API",
      description: "Restful APIs for looking network name and country",
      contact: {
        name: "Ebenezer Fiifi Ferguson",
        email: "ebbyferguson.guav@gmail.com",
      },
    },
  },
  apis: ["./routes/*.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
const app = express();
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

//Routes
const SearchRoute = require("./routes/search");

app.use("/search", SearchRoute);

startUpdebug("Application Name: MCC-LOOKUP WEBSERVICE - TESTING");

const port = process.env.PORT || 3030;
app.listen(port, () => startUpdebug(`Listening on Port ${port}`));
