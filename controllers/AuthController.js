const User = require("../models/usermodel");
const debug = require("debug")("app:debug");
const jwt = require("jsonwebtoken");
const Helper = require("../utils/helper");

const register = async (req, res, next) => {
  let newUser = {
    username: req.body.username,
    password: req.body.password,
  };

  var username = req.body.username;
  var password = req.body.password;

  let token = jwt.sign(newUser, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "4h",
  });
  let refreshToken = jwt.sign(newUser, process.env.REFRESH_TOKEN_SECRET);
  res.status(200).send({
    hostHeaderInfo: {
      responseCode: `000`,
      responseMessage: "SUCCESS",
    },
    body: {
      username: username,
      token: token,
      refreshToken: refreshToken,
    },
  });
};

module.exports = { register };
