const debug = require("debug")("app:debug");
const Helper = require("../utils/Helper");
("use strict");
const fs = require("fs");
const { response } = require("express");

var jsonData;

fs.readFile("data.json", (err, data) => {
  if (err) throw err;
  jsonData = JSON.parse(data);
});

const getAllNetworksByCountry = async (req, res, next) => {
  let country = req.params.country;
};

const getAllNetworksByMccOrCountry = async (req, res, next) => {
  let mcc = req.params.value;

  var obj = jsonData.country;
  var results = [];
  var networks = [];

  //   for (var i = 0; i < obj.length; i++) {
  //     if (obj[i].name == value || obj[i].mcc == value) {
  //       results.push(obj[i]);
  //     }
  //   }

  //   for (var i = 0; i < obj.length; i++){
  //     if (results[i].name == value || results[i].mcc == value) {
  //       results.push(obj[i]);
  //     }
  //   }
  //   if (results.length < 1) {
  //     return res.status(404).send({ responseMessage: "RECORD NOT FOUND" });
  //   } else {
  //     return res
  //       .status(200)
  //       .send({ network: results[0].network, country: results[0].name });
  //   }
};

const getNetworkAndCountry = async (req, res, next) => {
  let mnc = req.params.mnc;
  let mcc = req.params.mcc;

  var obj = jsonData.country;
  var results = [];

  for (var i = 0; i < obj.length; i++) {
    if (obj[i].mcc == mcc && obj[i].mnc == mnc) {
      results.push(obj[i]);
    }
  }
  if (results.length < 1) {
    return res.status(404).send({ responseMessage: "RECORD NOT FOUND" });
  } else {
    return res
      .status(200)
      .send({ network: results[0].network, country: results[0].name });
  }
};

module.exports = {
  // test,
  getAllNetworksByMccOrCountry,
  getNetworkAndCountry,
};
