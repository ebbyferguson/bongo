const jwt = require("jsonwebtoken");

const authenticate = (req, res, next) => {
  try {
    let authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];
    const decode = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.user = decode;
    next();
  } catch (error) {
    return res.status(401).send({
      hostHeaderInfo: {
        responseCode: `A01`,
        responseMessage: "AUTHENTICATION FAILED!",
      },
    });
  }
};

module.exports = authenticate;
