const express = require("express");
const router = express.Router();

router.use(express.json());

const SearchController = require("../controllers/SearchController");

//get network name (request params mnc && mcc)
router.get("/:mnc/:mcc", SearchController.getNetworkAndCountry);

//get all networks (request params country)
router.get("/:value", SearchController.getAllNetworksByMccOrCountry);



module.exports = router;
