function Country(name, mcc, mnc, iso, code, network) {
  this.name = name;
  this.mcc = mcc;
  this.mnc = mnc;
  this.iso = iso;
  this.code = code;
  this.network = network;
}
